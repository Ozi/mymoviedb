//
//  MovieViewModel.swift
//  MyMovieDB
//
//  Created by Muchamad Fauzi on 09/05/23.
//

import Foundation

class MovieViewModel {
    func getPopularList(onSuccess: @escaping([MovieItems]) -> Void ) {
        APICaller.shared.getPopular { result in
            switch result {
            case .success(let titles):
                onSuccess(titles)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func getUpcomingList(onSuccess: @escaping([MovieItems]) -> Void) {
        APICaller.shared.getUpcomingMovies { result in
            switch result {
            case .success(let titles):
                onSuccess(titles)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func getTopRatedList(onSuccess: @escaping([MovieItems]) -> Void) {
        APICaller.shared.getTopRated { result in
            switch result {
            case .success(let titles):
                onSuccess(titles)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func getTrendingList(onSuccess: @escaping([MovieItems]) -> Void) {
        APICaller.shared.getTrendingMovies { result in
            switch result {
            case .success(let titles):
                onSuccess(titles)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func configureHeaderView(onSuccess: @escaping([MovieItems]) -> Void) {
        APICaller.shared.getTrendingMovies { result in
            switch result {
            case .success(let titles):
                onSuccess(titles)
            case .failure(let erorr):
                print(erorr.localizedDescription)
            }
        }
    }
}
