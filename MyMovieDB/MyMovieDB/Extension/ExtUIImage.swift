//
//  ExtUIImage.swift
//  MyMovieDB
//
//  Created by Muchamad Fauzi on 09/05/23.
//

import UIKit

extension UIImage {
    func resize(to size: CGSize) -> UIImage? {
        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { _ in
            self.draw(in: CGRect(origin: .zero, size: size))
        }
    }
}
