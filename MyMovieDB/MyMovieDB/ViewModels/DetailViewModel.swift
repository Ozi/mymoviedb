//
//  DetailViewModel.swift
//  MyMovieDB
//
//  Created by Muchamad Fauzi on 09/05/23.
//

import Foundation

class DetailViewModel {
    var id: Int = 0
    var movieItem: MovieItems?
    
    func getDetailMovie(onSuccess: @escaping(MovieItems) -> Void) {
        APICaller.shared.getDetail(id: id) { result in
            switch result {
            case .success(let movie):
                self.movieItem = movie
                onSuccess(movie)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
}
