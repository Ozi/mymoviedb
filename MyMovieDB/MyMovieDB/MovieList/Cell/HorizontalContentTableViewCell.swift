//
//  HorizontalContentTableViewCell.swift
//  MyMovieDB
//
//  Created by Muchamad Fauzi on 09/05/23.
//

import UIKit

protocol HorizontalContentTableViewCellDelegate: AnyObject {
    func horizontalContentTableViewCellDidTapCell(_ id: Int)
}

class HorizontalContentTableViewCell: UITableViewCell {

    static let identifier = "HorizontalContentTableViewCell"
    
    weak var delegate: HorizontalContentTableViewCellDelegate?
    
    private var titles: [MovieItems] = [MovieItems]()
    
    private var isLoadingData = false
    
    private let collectionView: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 20, bottom: 0, right: 0)
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)

        collectionView.register(TitleCollectionViewCell.self, forCellWithReuseIdentifier: TitleCollectionViewCell.identifier)
        return collectionView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(collectionView)
        collectionView.backgroundColor = UIColor(hexString: "#25272A")
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.frame = contentView.bounds
    }
    
    public func configure(with titles: [MovieItems], page: Int, totalPages: Int) {
        if !isLoadingData && page >= totalPages - 1 {
            isLoadingData = true
  
            self.titles.append(contentsOf: titles)
            
            DispatchQueue.main.async { [weak self] in
                self?.collectionView.reloadData()
            }
        }
        
        self.titles = titles
        
        DispatchQueue.main.async { [weak self] in
            self?.collectionView.reloadData()
        }
    }
}

extension HorizontalContentTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TitleCollectionViewCell.identifier, for: indexPath) as? TitleCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        guard let model = titles[indexPath.row].poster_path else {
            return UICollectionViewCell()
        }
        guard let titleName = titles[indexPath.row].title else {
            return UICollectionViewCell()
        }
        cell.configure(with: model, title: titleName)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let movie = titles[indexPath.row]
        guard let id = movie.id else {
            return
        }
        
        delegate?.horizontalContentTableViewCellDidTapCell(id)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:
    UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 140, height: 200)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
//        print("loading scroll")
        let offsetX = scrollView.contentOffset.x
        let contentWidth = scrollView.contentSize.width
        let width = scrollView.frame.size.width
        
        if offsetX > contentWidth - width {
            print("mentok")
//            isLoadingData = true
            
//            self.collectionView.refreshControl = UIRefreshControl()
//            self.collectionView.refreshControl?.tintColor = .white
//            self.collectionView.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
            
            self.titles.append(contentsOf: titles)
            DispatchQueue.main.async { [weak self] in
                self?.collectionView.reloadData()
            }
        }
    }
    
}
