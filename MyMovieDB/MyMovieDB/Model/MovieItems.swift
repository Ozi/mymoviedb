//
//  MovieItems.swift
//  MyMovieDB
//
//  Created by Muchamad Fauzi on 09/05/23.
//

import Foundation

// MovieResponse
struct MovieResponse: Codable {
    let results: [MovieItems]
}

// Genre
struct genre: Decodable {
    var id: Int
    var name: String
}
struct MovieGenre: Decodable {
    let genres: [genre]
    
    enum CodingKeys: String, CodingKey {
        case genres = "genres"
    }
}
// MovieItems
struct MovieItems: Codable {
    let id: Int?
    let media_type: String?
    let title: String?
    let poster_path: String?
    let overview: String?
    let vote_count: Int
    let release_date: String?
    let vote_average: Double
}
