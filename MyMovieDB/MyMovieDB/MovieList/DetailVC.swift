//
//  DetailVC.swift
//  MyMovieDB
//
//  Created by Muchamad Fauzi on 09/05/23.
//

import UIKit
import SDWebImage

class DetailVC: UIViewController {
    
    private let viewModel: DetailViewModel = DetailViewModel()
    
    private let backButton: UIButton = {
        let backButton = UIButton()
        backButton.setTitle("Back", for: .normal)
        backButton.setTitleColor(.blue, for: .normal)
        backButton.translatesAutoresizingMaskIntoConstraints = false
        
        return backButton
    }()
    
    private let scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let scrollStackViewContainer: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let movieImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleToFill
        image.backgroundColor = UIColor(hexString: "#25272A")
        image.heightAnchor.constraint(equalToConstant: 500).isActive = true
        
        return image
    }()
    
    private let titleLabel: UILabel = {
        
        let label = UILabel()
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 22, weight: .bold)
        label.text = "Harry potter"
        return label
    }()
    
    private let overviewLabel: UILabel = {
        
        let label = UILabel()
        label.textColor = .white
        label.font = .systemFont(ofSize: 18, weight: .regular)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.text = "This is the best movie ever to watch as a kid!"
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexString: "#25272A")
        
        viewModel.getDetailMovie { movie in
            DispatchQueue.main.async {
                self.titleLabel.text = movie.title
                self.overviewLabel.text = movie.overview
                self.movieImageView.sd_setImage(with: URL(string: Constants.imageUrl + (self.viewModel.movieItem?.poster_path ?? "")))
            }
        }
        setupScrollView()
        setupView()
        
    }
    
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setupScrollView() {
        let margins = view.layoutMarginsGuide
        
        view.addSubview(backButton)
        backButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        backButton.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
        backButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        view.addSubview(scrollView)
        scrollView.addSubview(scrollStackViewContainer)
        
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: backButton.bottomAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
        scrollStackViewContainer.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        scrollStackViewContainer.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        scrollStackViewContainer.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        scrollStackViewContainer.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        scrollStackViewContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
    }
    
    func setMovieId(id: Int) {
        viewModel.id = id
    }
    
    func setupView() {
        scrollStackViewContainer.addArrangedSubview(movieImageView)
        movieImageView.topAnchor.constraint(equalTo: scrollStackViewContainer.topAnchor).isActive = true
        movieImageView.leadingAnchor.constraint(equalTo: scrollStackViewContainer.leadingAnchor).isActive = true
        movieImageView.trailingAnchor.constraint(equalTo: scrollStackViewContainer.trailingAnchor).isActive = true
        
        scrollStackViewContainer.addArrangedSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: movieImageView.bottomAnchor, constant: 5).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: scrollStackViewContainer.leadingAnchor, constant: 10).isActive = true
        
        scrollStackViewContainer.addArrangedSubview(overviewLabel)
        overviewLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 15).isActive = true
        overviewLabel.leadingAnchor.constraint(equalTo: scrollStackViewContainer.leadingAnchor, constant: 10).isActive = true
        overviewLabel.trailingAnchor.constraint(equalTo: scrollStackViewContainer.trailingAnchor).isActive = true
    }
    
}
