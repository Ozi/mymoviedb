//
//  MovieVC.swift
//  MyMovieDB
//
//  Created by Muchamad Fauzi on 09/05/23.
//

import UIKit

enum Sections: Int {
    case Popular = 0
    case ComingSoon = 1
    case TopRated = 2
    case TrendingMovies = 3
}

class MovieVC: UIViewController {
    
    private var headerView: HeaderView?
    
    private let viewModel: MovieViewModel = MovieViewModel()
    
    var currentPage: Int = 1
    var totalPage: Int = 0
    
    let sectionTitles: [String] = ["Popular Movies", "Coming Soon", "Top rated", "Trending Movies"]
    
    private let navbarView: UIView = {
        let navbar = UIView()
        navbar.backgroundColor = .clear
        navbar.translatesAutoresizingMaskIntoConstraints = false
        
        return navbar
    }()
    
    private let titleAppLabel: UILabel = {
        let t = UILabel()
        t.translatesAutoresizingMaskIntoConstraints = false
        t.text = "MyMovieDB"
        t.font = UIFont.boldSystemFont(ofSize: 28)
        t.textColor = .white
        
        return t
    }()
    
    private let homeFeedTable: UITableView = {
        let table = UITableView(frame: .zero, style: .grouped)
        table.register(HorizontalContentTableViewCell.self, forCellReuseIdentifier: HorizontalContentTableViewCell.identifier)
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexString: "#25272A")
        view.addSubview(navbarView)
        navbarView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        navbarView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        navbarView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        navbarView.heightAnchor.constraint(equalToConstant: 56).isActive = true
        
        navbarView.addSubview(titleAppLabel)
        titleAppLabel.centerYAnchor.constraint(equalTo: navbarView.centerYAnchor).isActive = true
        titleAppLabel.leadingAnchor.constraint(equalTo: navbarView.leadingAnchor, constant: 23).isActive = true
        titleAppLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
        titleAppLabel.widthAnchor.constraint(equalToConstant: 170).isActive = true
        
        view.addSubview(homeFeedTable)
        tableViewContraints()
        homeFeedTable.backgroundColor = .clear
        homeFeedTable.delegate = self
        homeFeedTable.dataSource = self
        
        headerView = HeaderView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 261))
        homeFeedTable.tableHeaderView = headerView
        configureHeaderView()
        
        homeFeedTable.refreshControl = UIRefreshControl()
        homeFeedTable.refreshControl?.tintColor = .white
        homeFeedTable.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    
    @objc func refresh() {
        print("refresh")

        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
            self.homeFeedTable.refreshControl?.endRefreshing()
        }
        
        configureHeaderView()
        homeFeedTable.delegate = self
        homeFeedTable.dataSource = self
    }
    
    private func configureHeaderView() {
        viewModel.configureHeaderView { headerImage in
            self.headerView?.configure(with: headerImage)
        }
    }
    
    func tableViewContraints() {
        homeFeedTable.topAnchor.constraint(equalTo: navbarView.bottomAnchor).isActive = true
        homeFeedTable.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        homeFeedTable.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        homeFeedTable.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
}

extension MovieVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HorizontalContentTableViewCell.identifier, for: indexPath) as? HorizontalContentTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        
        switch indexPath.section {
        case Sections.Popular.rawValue:
            viewModel.getPopularList { [self] movies in
                cell.configure(with: movies, page: currentPage, totalPages: totalPage)
            }
            
        case Sections.ComingSoon.rawValue:
            viewModel.getUpcomingList { [self] movies in
                cell.configure(with: movies, page: currentPage, totalPages: totalPage)
            }
            
        case Sections.TopRated.rawValue:
            viewModel.getTopRatedList { [self] movies in
                cell.configure(with: movies, page: currentPage, totalPages: totalPage)
            }
            
        case Sections.TrendingMovies.rawValue:
            viewModel.getTrendingList { [self] movies in
                cell.configure(with: movies, page: currentPage, totalPages: totalPage)
            }
            
        default:
            return UITableViewCell()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else {return}
        header.textLabel?.font = .systemFont(ofSize: 20, weight: .semibold)
        header.textLabel?.frame = CGRect(x: header.bounds.origin.x, y: header.bounds.origin.y, width: 100, height: header.bounds.height)
        header.textLabel?.textColor = .white
        header.textLabel?.text = header.textLabel?.text?.localizedCapitalized
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let defaultOffset = view.safeAreaInsets.top
        let offset = scrollView.contentOffset.y + defaultOffset
        
        navigationController?.navigationBar.transform = .init(translationX: 0, y: min(0, -offset))
    }
}

extension MovieVC: HorizontalContentTableViewCellDelegate {
    func horizontalContentTableViewCellDidTapCell(_ id: Int) {
        DispatchQueue.main.async { [weak self] in
            let vc = DetailVC()
            vc.setMovieId(id: id)
            self?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
