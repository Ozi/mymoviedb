//
//  TitleCollectionViewCell.swift
//  MyMovieDB
//
//  Created by Muchamad Fauzi on 09/05/23.
//

import UIKit
import SDWebImage

class TitleCollectionViewCell: UICollectionViewCell {

    static let identifier = "TitleCollectionViewCell"
    
    private let posterImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private let titleNameLabel: UILabel = {
        let title = UILabel()
        title.textColor = .white
        title.numberOfLines = 2
        title.font = UIFont.boldSystemFont(ofSize: 14)
        return title
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(posterImageView)
        contentView.addSubview(titleNameLabel)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        posterImageView.frame = CGRect(x: 0, y: -20, width: 140, height: 200)
        titleNameLabel.frame = CGRect(x: 0, y: posterImageView.frame.maxY, width: 140, height: 50)
    }
    
    
    public func configure(with model: String, title: String) {
        
        guard let url = URL(string: "https://image.tmdb.org/t/p/w500/\(model)") else {
            return
        }
        titleNameLabel.text = title
        posterImageView.sd_setImage(with: url, completed: nil)
    }
    
}
